
package main 


import (
	"fmt"
	"strings"	
	"strconv"
)

func main() {
	name := askName()

	// verificar se o suspect é uma string
	vn := validateString(name)

	if !vn{
		fmt.Println("Não sabe nem o que é um nome")
		return
	}
	
	// verifica se o suspect respeita a RN1 (vogais)
	vv := validateVogals(name)

	if !vv{
		fmt.Println("Não o que vem a seguir: Suspeito! Forte! Foi você!")
		return
	}

	biogender := getBioGender(name)
	weapon := getWeapon(name)

	time := ""
	
	if biogender == "Male"{
		fmt.Print("Que horas você estava no saguão? (hh:mm)")
		fmt.Scan(&time)
	}
	
	vw := validateAssassinWoman(biogender, weapon)
	vm := validateAssassinMan(biogender, time)

	if !(vw || vm){
		fmt.Println("Não o que vem a seguir: Suspeito! Forte! Foi você!")
		return
	}
	fmt.Println("ASSSSAAAAASSSSSIIIIINOOOOO(AAAAA)!!!!!!!!!!!!!!!!!!!!!!!!!!")
}

func askName() string{
	suspect := ""
    fmt.Print("Digite o nome da pessoa: ")
	fmt.Scan(&suspect)
	return suspect
}

func validateString(suspect string) bool{
    return ! isNumeric(suspect)
}

func isNumeric(s string) bool {
    _, err := strconv.ParseFloat(s, 64)
    return err == nil
}

func validateVogals(suspect string) bool{
    counter := 0
    for _,letter := range suspect{
		for _,vogal := range "aei"{
			if letter==vogal {
				counter += 1
			}
		}
	}
    return counter >= 3  && (!strings.ContainsAny(strings.ToLower(suspect),"o") || !strings.ContainsAny(strings.ToLower(suspect),"u") )
}

func getBioGender(suspect string) string{
	var mans = []string{"renato faca", "roberto arma", "uesllei"}
	for _,man := range mans{
		if strings.ToLower(suspect) == man{
			return "Male"
		}
	}
    return "Female"
}

func getWeapon(suspect string) bool{
	var whiteWeapons = []string{"faca", "corda", "calice"}
    hasWeapon := false
    for _,weapon := range whiteWeapons{
        if strings.ContainsAny(strings.ToLower(suspect),weapon){
            hasWeapon = true
		}
	}
    return hasWeapon
}

func validateAssassinWoman(biogender string, weapon bool) bool{
    return biogender == "Female" && weapon
}

func validateAssassinMan(biogender string, time string) bool{
    return biogender == "Male" && time == "00:30"
}
